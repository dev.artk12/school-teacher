package com.religiousschool;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

public class InputStudiName extends DialogFragment implements TextWatcher {


    private static final String NUMBERKEY = "NumberKey";
    private static final String CONTERKEY =  "ConterKey";

    AppCompatTextView numbetStock;
    AppCompatEditText studentNumbers , educationName;
    AppCompatButton done;
    detailstudiCallBack callback;

    @Override
    public void onStart() {
        super.onStart();

        Window window = getDialog().getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (detailstudiCallBack) context;
    }

    public static InputStudiName newInstance(String number , int conter) {
        
        Bundle args = new Bundle();
        
        InputStudiName fragment = new InputStudiName();
        args.putString(NUMBERKEY , number);
        args.putInt(CONTERKEY , conter);

        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.inpudstudinamedialog , container , false);
        numbetStock = (AppCompatTextView) view.findViewById(R.id.number);
        studentNumbers = (AppCompatEditText) view.findViewById(R.id.studentinput);
        educationName = (AppCompatEditText) view.findViewById(R.id.studiinput);
        done = (AppCompatButton) view.findViewById(R.id.done);

        numbetStock.setText(""+getArguments().getInt(CONTERKEY));
        numbetStock.append(" از ");
        numbetStock.append(""+getArguments().getString(NUMBERKEY));



        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        callback.DoneButtonCallBack(done);
        callback.InputsCallBack( educationName.getText().toString() , studentNumbers.getText().toString());

        educationName.addTextChangedListener(this);
        studentNumbers.addTextChangedListener(this);


        return view;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        callback.InputsCallBack( educationName.getText().toString() , studentNumbers.getText().toString());

    }
}
