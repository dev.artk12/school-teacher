package com.religiousschool;


import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutCompat;
import android.widget.RelativeLayout;

import de.hdodenhof.circleimageview.CircleImageView;

public interface CameraBtn {

    void Camera(AppCompatButton request, CircleImageView profile , LinearLayoutCompat btnparent , AppCompatImageButton camera , AppCompatImageButton gallery);
    void ContentCallBack (String Name , String Kode , String LastName , String education , String Stock);


}
