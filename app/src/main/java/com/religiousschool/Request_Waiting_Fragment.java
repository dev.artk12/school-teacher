package com.religiousschool;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

public class Request_Waiting_Fragment extends Fragment {

    TypeWriter text;
    AppCompatEditText kode;
    WindowManager windowManager;
    DisplayMetrics metrics;
    AppCompatButton done;
    WaitingCallBack callBack;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBack = (WaitingCallBack) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.request_waiting_fragment, container , false);

        text = (TypeWriter) view.findViewById(R.id.text);
        kode = (AppCompatEditText) view.findViewById(R.id.kode);
        done = (AppCompatButton) view.findViewById(R.id.done);

        windowManager = getActivity().getWindowManager();
        metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);

        kode.getLayoutParams().width =  (int) (metrics.widthPixels / 1.5);

        text.setCharacterDelay(150);
        text.animateText("ٱلْحَمْدُ لِلَّه رَبِ ٱلْعَٰلَمِين.... ");

        callBack.DoneButton(done);



        return view;
    }
}
