package com.religiousschool;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class InputGradesItemList extends RecyclerView.Adapter<InputGradesItemList.ViewHolder>{


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gradesinputlistitem,viewGroup,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    protected class ViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView Name , KodeMelli , FatherName;
        AppCompatEditText grades;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            KodeMelli = (AppCompatTextView) itemView.findViewById(R.id.KodeMelli);
            FatherName = (AppCompatTextView)itemView.findViewById(R.id.fatherName);
            Name = (AppCompatTextView) itemView.findViewById(R.id.name);
            grades = itemView.findViewById(R.id.grades);
        }
    }




}
