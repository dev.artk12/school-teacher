package com.religiousschool;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.model.AspectRatio;
import com.yalantis.ucrop.view.CropImageView;

import java.io.File;

public class Ucrop {

    private static int color;

    public Ucrop(int color){
    this.color = color;
    }

    private static UCrop config(UCrop uCrop){

        UCrop.Options options = new UCrop.Options();

        options.setCompressionQuality(100);
        options.setCircleDimmedLayer(true);
        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
        options.setHideBottomControls(false);
        options.setCropGridStrokeWidth(1);
        options.setCropGridColor(color);
        options.setCropGridColumnCount(0);
        options.setCropGridRowCount(0);
        options.setToolbarColor(color);
        options.setActiveWidgetColor(color);
        options.setAspectRatioOptions(1,
                new AspectRatio("WOW", 2,1),
                new AspectRatio("MUCH", 3, 4),
                new AspectRatio("RATIO", CropImageView.DEFAULT_ASPECT_RATIO, CropImageView.DEFAULT_ASPECT_RATIO),
                new AspectRatio("SO", 16, 9),
                new AspectRatio("ASPECT", 1, 1));

        return uCrop.withOptions(options);
    }

    private static UCrop base(UCrop uCrop){
        float ratioX = 2;
        float ratioY = 2;
        if (ratioX > 0 && ratioY > 0) {
            uCrop = uCrop.withAspectRatio(ratioX, ratioY);
        }
        int maxWidth = 250;
        int maxHeight = 250;
        if (maxWidth > 0 && maxHeight > 0) {
            uCrop = uCrop.withMaxResultSize(maxWidth, maxHeight);
        }
        return uCrop;
    }
    public void startCropActivity(Activity context,@NonNull Uri uri , String filname) {

            filname += ".png";
            UCrop uCrop = UCrop.of(uri,Uri.fromFile(new File(context.getCacheDir(),filname)));

            uCrop = base(uCrop);
            uCrop = config(uCrop);

            uCrop.start(context);


    }
    public static void openGalletry(Activity context , int REQUEST_SELECT_PICTURE){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        context.startActivityForResult(Intent.createChooser(intent, ("choose ur photo")), REQUEST_SELECT_PICTURE);
    }
    public static void captureImage(Activity activity,Uri uri , int REQUEST_CAPTURE_IMAGE ) {

        Intent CamIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "file"+String.valueOf(System.currentTimeMillis())+".jpg");
        uri = Uri.fromFile(file);
        CamIntent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
        CamIntent.putExtra("return-data",true);
        activity.startActivityForResult(CamIntent,REQUEST_CAPTURE_IMAGE);
    }
    public static Bitmap handleCropResult(Context context,@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        Bitmap bitmap ;
        if (resultUri != null) {
           bitmap = BitmapFactory.decodeFile(resultUri.getPath());
           new Output_files(context).writecurrentprofile(bitmap);
        } else {
            Toast.makeText(context, result.toString(), Toast.LENGTH_SHORT).show();
            return null;
        }
        return bitmap;
    }
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public static void handleCropError(@NonNull Intent result, Context context) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(context, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
        }
    }

}
