package com.religiousschool;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class InputStudentFragment extends Fragment {

    private static final String STUDINAMEKEY = "StudeName";
    private static final String STUDICOUNTERKEY = "StudentCounter" ;

    AppCompatEditText KodeMelli , FirstName , LastName ,FatherName;
    AppCompatButton done;
    AppCompatTextView counter , title;
    InputStudentCallBack callBack;

    public static InputStudentFragment newInstance(String StudiName , int StudentCounter) {

        Bundle args = new Bundle();

        InputStudentFragment fragment = new InputStudentFragment();
        fragment.setArguments(args);
        args.putString(STUDINAMEKEY , StudiName );
        args.putInt(STUDICOUNTERKEY , StudentCounter );

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBack = (InputStudentCallBack) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view  = inflater.inflate(R.layout.inputstudentsfragment , container , false);

        KodeMelli = (AppCompatEditText) view.findViewById(R.id.KodeMelli);
        FirstName = (AppCompatEditText) view.findViewById(R.id.firstname);
        LastName  = (AppCompatEditText) view.findViewById(R.id.lastname);
        FatherName = (AppCompatEditText) view.findViewById(R.id.fatherName);
        done = (AppCompatButton) view.findViewById(R.id.next);
        counter = (AppCompatTextView) view.findViewById(R.id.studentcounter);
        title = (AppCompatTextView) view.findViewById(R.id.educationname);

        title.setText(getArguments().getString(STUDINAMEKEY));

        counter.setText("دانش آموز شماره "+getArguments().getInt(STUDICOUNTERKEY));

        callBack.nextButtonCallBack(done);

        return view;
    }
}
