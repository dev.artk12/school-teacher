package com.religiousschool;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , profileFragmnetCallBack {


    private static final String MAINPROFILE_IMAGEVIEW = "201";
    private static final String MAINCAMERABUTTONS = "202";

    private static final String PROFILE_IMAGEVIEW = "prof";
    private static final String CAMERABUTTONS = "btns";
    private static final String MAINNAMEKEY = "NameKey" ;
    private static final String MAINKODEKEY = "KodeKey";
    private static final String MAINLASTNAMEKEY = "LastNameKey" ;
    private static final String MAINEDUCATIONKEY = "EducationKey";
    private static final String STOCKKEY = "StockKey";
    private static final String EDUCCATIONNAMEKEY = "EducationName";
    private static final int MAINREQUEST_IMAGE = 2002;



    ViewPager viewPager;
    TabLayout tabLayout;
    DrawerLayout drawer;
    Toolbar toolbar;
    ActionBarDrawerToggle toggle;
    PagerAdapter adapter;
    AppCompatImageButton searchIcon;
    int color , ucropcolor;
    Fragment fragment;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    Class fragmentclass;
    Map<String,View> viewMap;
    Map<String,String> result;
    private int MAINREQUEST_OPEN_GALLETY = 1001;
    Uri uri;
    LinearLayoutCompat newexam;

    String [] mainpermission = new String[]{Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE , Manifest.permission.READ_EXTERNAL_STORAGE};
    String maincamraPermisson = Manifest.permission.CAMERA;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        color = ContextCompat.getColor(this,R.color.seariconmainactivity);
        ucropcolor = ContextCompat.getColor(this,R.color.Base);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        searchIcon = (AppCompatImageButton) findViewById(R.id.ic_search);

        newexam = (LinearLayoutCompat) findViewById(R.id.newexamL);

        viewMap = new HashMap<>();
        result = new HashMap<>();

        searchIcon.getDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        drawerAndstartersetup();

        adapter = new PagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(adapter);

        newexam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "new Exam", Toast.LENGTH_SHORT).show();
            }
        });




    }

    private void drawerAndstartersetup() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        fragment = null ;
        fragmentclass = null;
        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();

        if (id == R.id.prof) {
            fragmentclass = ProfileFragment.class;
        } else if (id == R.id.newexam) {
            fragmentclass = ExamInput.class;
        } else if (id == R.id.exams) {
            fragmentclass = exams_Fragment.class;
        } else if (id == R.id.schedule){

        } else if (id == R.id.studentgroup) {

        } else if (id == R.id.aboutus) {

        }

        if (fragmentclass == ExamInput.class){

            try {
                fragment = (DialogFragment) fragmentclass.newInstance();
                ((DialogFragment) fragment).show(getSupportFragmentManager(),"ExamInput");

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }

        }else if (fragmentclass != null){

        try {
            fragment = (Fragment) fragmentclass.newInstance();
            transaction.addToBackStack("myscreen");
            transaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_in);
            transaction.replace(R.id.drawer_layout,fragment);
            transaction.commit();
        }catch (InstantiationException e){
            e.printStackTrace();
        }catch (IllegalAccessException e){
            e.printStackTrace();
        }
    }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void ProfileCameraFragment(AppCompatButton request, final CircleImageView profile, final LinearLayoutCompat btnparent, AppCompatImageButton camera, AppCompatImageButton gallery) {
        btnparent.setVisibility(View.INVISIBLE);
        final Animation ani = AnimationUtils.loadAnimation(MainActivity.this,R.anim.cameragalleryout);


        viewMap.put(MAINPROFILE_IMAGEVIEW,profile);
        viewMap.put(MAINCAMERABUTTONS , btnparent);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OpenCameraButtons(btnparent, profile, ani);

            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(MainActivity.this , maincamraPermisson) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(MainActivity.this , Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                    CameraRequest();
                }else {
                    ActivityCompat.requestPermissions(MainActivity.this , mainpermission, 101);
                }
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(MainActivity.this , Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(MainActivity.this , Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Ucrop.openGalletry(MainActivity.this, MAINREQUEST_OPEN_GALLETY);
                }else {
                    ActivityCompat.requestPermissions(MainActivity.this , mainpermission, 102);
                }

            }
        });

        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (result.get(MAINNAMEKEY) != null && result.get(MAINKODEKEY)!= null
                        &&result.get(MAINLASTNAMEKEY)!= null &&
                        result.get(MAINEDUCATIONKEY)!= null ){

                    if (!result.get(MAINNAMEKEY).isEmpty() && !result.get(MAINKODEKEY).isEmpty()
                            &&!result.get(MAINLASTNAMEKEY).isEmpty() &&
                            !result.get(MAINEDUCATIONKEY).isEmpty() ){

                        //REQUEST......


                    }else {
                        Toast.makeText(MainActivity.this, "لطفا تمامی فیلد ها را بر کنید", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(MainActivity.this, "لطفا تمامی فیلدها را بر کنید", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Ucrop ucrop = new Ucrop(ucropcolor);
        if (requestCode == MAINREQUEST_IMAGE && resultCode == RESULT_OK){
            ucrop.startCropActivity(MainActivity.this,uri,"profile");
        }else if (requestCode == MAINREQUEST_OPEN_GALLETY && resultCode == RESULT_OK){
            uri = data.getData();
            ucrop.startCropActivity(MainActivity.this,uri,"profile");
        }else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK){
            Bitmap bitmap = Ucrop.handleCropResult(MainActivity.this, data);
            CircleImageView profile = (CircleImageView) viewMap.get(MAINPROFILE_IMAGEVIEW);
            profile.setImageBitmap(bitmap);
            CloseCameraButtons((LinearLayoutCompat) viewMap.get(MAINCAMERABUTTONS),profile);


        }else if(requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_CANCELED){
            Toast.makeText(this, "cancel", Toast.LENGTH_SHORT).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void CameraRequest() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent CamIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()+"/inventory/Image/",
                "file"+String.valueOf(System.currentTimeMillis())+".jpg");
        uri = Uri.fromFile(file);
        CamIntent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
        CamIntent.putExtra("return-data",true);
        startActivityForResult(CamIntent,MAINREQUEST_IMAGE);
    }
    private void OpenCameraButtons(LinearLayoutCompat btnparent, CircleImageView profile, Animation ani) {
        Animation animation = AnimationUtils.loadAnimation(MainActivity.this,R.anim.cameragalleryin);
        btnparent.setAnimation(animation);
        btnparent.setVisibility(View.VISIBLE);
        profile.setAnimation(ani);
    }

    private void CloseCameraButtons(LinearLayoutCompat btnparent, CircleImageView profile) {
        Animation animation = AnimationUtils.loadAnimation(MainActivity.this,R.anim.cameragalleryout);
        btnparent.setAnimation(animation);
        btnparent.setVisibility(View.INVISIBLE);
        Animation anim = AnimationUtils.loadAnimation(MainActivity.this,R.anim.cameragalleryin);
        profile.setAnimation(anim);
    }

    @Override
    public void profilefragmnetContentCallBack(String Name, String Kode, String LastName, String education) {

    }

    class PagerAdapter extends FragmentPagerAdapter{


        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {

            switch (i){
                case 1 : return new SearchFragment();
                case 0 : return new MainFragment();

            }


            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
