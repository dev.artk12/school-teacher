package com.religiousschool;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import net.steamcrafted.lineartimepicker.dialog.LinearTimePickerDialog;

public class studidetailfragmnet extends Fragment implements CompoundButton.OnCheckedChangeListener, TextWatcher {

    private static final String NUMBERKEY = "NamberKey";
    private static final String COUNTERKEY = "CounterKey";
    String number ;
    int colorblue,colorbg,counter , txtbg;
    detailstudiCallBack callback;
    AppCompatButton done;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (detailstudiCallBack) context;
    }

    public static studidetailfragmnet newInstance(String number , int counter) {

        Bundle args = new Bundle();

        studidetailfragmnet fragment = new studidetailfragmnet();
        fragment.setArguments(args);
        args.putString(NUMBERKEY , number);
        args.putInt(COUNTERKEY , counter);
        return fragment;
    }

    AppCompatTextView Number;
    LinearLayoutCompat Saturday‌Btn,SundayBtn,MondayBtn,TuesdayBtn,WednesdayBtn,ThursdayBtn;
    AppCompatCheckBox Saturday‌,Sunday,Monday,Tuesday,Wednesday,Thursday;
    AppCompatEditText Saturday‌Time,SundayTime,MondayTime,TuesdayTime,WednesdayTime,ThursdayTime , Students, EducationName;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rollcall,container,false);

        Number = (AppCompatTextView) view.findViewById(R.id.number);
        done = (AppCompatButton) view.findViewById(R.id.request);
        colorblue = ContextCompat.getColor(getContext(),R.color.Base);
        colorbg = ContextCompat.getColor(getContext(),R.color.backgroundPages);
        txtbg = ContextCompat.getColor(getContext(),R.color.hintregistercolor);

        Saturday‌Btn     = (LinearLayoutCompat) view.findViewById(R.id.SaturdayBtn);
        SundayBtn       = (LinearLayoutCompat) view.findViewById(R.id.SundayBtn);
        MondayBtn       = (LinearLayoutCompat) view.findViewById(R.id.MondayBtn);
        TuesdayBtn      = (LinearLayoutCompat) view.findViewById(R.id.TuesdayBtn);
        WednesdayBtn    = (LinearLayoutCompat) view.findViewById(R.id.WednesdayBtn);
        ThursdayBtn     = (LinearLayoutCompat) view.findViewById(R.id.ThursdayBtn);

        Saturday‌  = (AppCompatCheckBox) view.findViewById(R.id.Saturday);
        Sunday    = (AppCompatCheckBox) view.findViewById(R.id.Sunday);
        Monday    = (AppCompatCheckBox) view.findViewById(R.id.Monday);
        Tuesday   = (AppCompatCheckBox) view.findViewById(R.id.Tuesday);
        Wednesday = (AppCompatCheckBox) view.findViewById(R.id.Wednesday);
        Thursday  = (AppCompatCheckBox) view.findViewById(R.id.Thursday);

        Saturday‌Time    = (AppCompatEditText) view.findViewById(R.id.SaturdayTime);
        SundayTime      = (AppCompatEditText) view.findViewById(R.id.SundayTime);
        MondayTime      = (AppCompatEditText) view.findViewById(R.id.MondayTime);
        TuesdayTime     = (AppCompatEditText) view.findViewById(R.id.TuesdayTime);
        WednesdayTime   = (AppCompatEditText) view.findViewById(R.id.WednesdayTime);
        ThursdayTime    = (AppCompatEditText) view.findViewById(R.id.ThursdayTime);

        Students         = (AppCompatEditText) view.findViewById(R.id.students);
        EducationName    = (AppCompatEditText) view.findViewById(R.id.educationname);


        Saturday‌.setOnCheckedChangeListener(this);
        Sunday.setOnCheckedChangeListener(this);
        Monday.setOnCheckedChangeListener(this);
        Tuesday.setOnCheckedChangeListener(this);
        Wednesday.setOnCheckedChangeListener(this);
        Thursday.setOnCheckedChangeListener(this);


        callback.DoneButtonCallBack(done);

        callback.InputsCallBack(EducationName.getText().toString(),Students.getText().toString());

        Students.addTextChangedListener(this);
        EducationName.addTextChangedListener(this);


        number = getArguments().getString(NUMBERKEY);
        counter = getArguments().getInt(COUNTERKEY);

        Number.setText("درس "+counter);

        Saturday‌Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TimeDialog(Saturday‌Time);
            }
        });


        SundayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TimeDialog(SundayTime);
            }
        });


        MondayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TimeDialog(MondayTime);
            }
        });


        TuesdayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TimeDialog(TuesdayTime);
            }
        });


        WednesdayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TimeDialog(WednesdayTime);
            }
        });


        ThursdayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TimeDialog(ThursdayTime);
            }
        });

        return view;
    }

    private void TimeDialog(final AppCompatEditText edt) {
        LinearTimePickerDialog dialog = LinearTimePickerDialog.Builder.with(getContext()).
                setDialogBackgroundColor(colorblue).setPickerBackgroundColor(colorbg).setLineColor(colorblue).setShowTutorial(false).setButtonColor(colorbg)
                .setTextBackgroundColor(txtbg).setTextColor(ContextCompat.getColor(getContext(),R.color.textregistercolor))
                .setButtonCallback(new LinearTimePickerDialog.ButtonCallback() {
            @Override
            public void onPositive(DialogInterface dialog, int hour, int minutes) {
                String min = String.valueOf(minutes);
                if (minutes == 0){
                    min = "00";
                }
                edt.setText(hour+":"+min);
            }

            @Override
            public void onNegative(DialogInterface dialog) {

            }
        }).build();

        dialog.show();
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (Saturday‌.isChecked()){
            Saturday‌Btn.setEnabled(true);
        }if (!Saturday‌.isChecked()){
            Saturday‌Btn.setEnabled(false);
            Saturday‌Time.getText().clear();
        }

        if (Sunday.isChecked()){
            SundayBtn.setEnabled(true);
        }if (!Sunday.isChecked()){
            MondayBtn.setEnabled(false);
            SundayTime.getText().clear();
        }

        if (Monday.isChecked()){
            MondayBtn.setEnabled(true);
        }if (!Monday.isChecked()){
            MondayBtn.setEnabled(false);
            MondayTime.getText().clear();
        }

        if (Tuesday.isChecked()){
            TuesdayBtn.setEnabled(true);
        }if (!Tuesday.isChecked()){
            TuesdayBtn.setEnabled(false);
            TuesdayTime.getText().clear();
        }

        if (Wednesday.isChecked()){
            WednesdayBtn.setEnabled(true);
        }if (!Wednesday.isChecked()){
            WednesdayBtn.setEnabled(false);
            WednesdayTime.getText().clear();
        }

        if (Thursday.isChecked()){
            ThursdayBtn.setEnabled(true);
        }if (!Saturday‌.isChecked()){
            ThursdayBtn.setEnabled(false);
            ThursdayTime.getText().clear();
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        callback.InputsCallBack(EducationName.getText().toString(),Students.getText().toString());
    }
}
