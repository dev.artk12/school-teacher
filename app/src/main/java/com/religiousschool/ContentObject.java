package com.religiousschool;



public class ContentObject {

    private int ID ;
    private String Name;
    private String Stock;
    private String Creator;
    private String Model;
    private String Group_Name;
    private String Purchase_Price;
    private String Sale_Price;
    private String Encasement;
    private String BarCode;
    private String Description;
    private String Image;
    private String ImageAddress;
    private String StockFormat;
    private String SaleFormat;
    private String PurchaseFomat;
    private String calende;


    public String getCalende() {
        return calende;
    }

    public void setCalende(String calende) {
        this.calende = calende;
    }

    public String getPurchaseFomat() {
        return PurchaseFomat;
    }

    public void setPurchaseFomat(String purchaseFomat) {
        PurchaseFomat = purchaseFomat;
    }

    public String getSaleFormat() {
        return SaleFormat;
    }

    public void setSaleFormat(String saleFormat) {
        SaleFormat = saleFormat;
    }

    public String getStockFormat() {
        return StockFormat;
    }

    public void setStockFormat(String stockFormat) {
        StockFormat = stockFormat;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getStock() {
        return Stock;
    }

    public void setStock(String stock) {
        this.Stock = stock;
    }

    public String getCreator() {
        return Creator;
    }

    public void setCreator(String creator) {
        this.Creator = creator;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        this.Model = model;
    }

    public String getGroup_Name() {
        return Group_Name;
    }

    public void setGroup_Name(String group_Name) {
        this.Group_Name = group_Name;
    }

    public String getPurchase_Price() {
        return Purchase_Price;
    }

    public void setPurchase_Price(String purchase_Price) {
        this.Purchase_Price = purchase_Price;
    }

    public String getSale_Price() {
        return Sale_Price;
    }

    public void setSale_Price(String sale_Price) {
        this.Sale_Price = sale_Price;
    }

    public String getEncasement() {
        return Encasement;
    }

    public void setEncasement(String encasement) {
        this.Encasement = encasement;
    }

    public String getBarCode() {
        return BarCode;
    }

    public void setBarCode(String barCode) {
        this.BarCode = barCode;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        this.Image = image;
    }

    public String getImageAddress() {
        return ImageAddress;
    }

    public void setImageAddress(String imageAddress) {
        ImageAddress = imageAddress;
    }
}
