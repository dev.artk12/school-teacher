package com.religiousschool;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import de.hdodenhof.circleimageview.CircleImageView;

public class Register extends Fragment implements TextWatcher {


    AppCompatEditText name , kode , lastname , education , stock ;
    AppCompatImageButton  gallery , camera ;
    CircleImageView profile;
    WindowManager windowManager;
    DisplayMetrics metrics;
    CameraBtn callback;
    LinearLayoutCompat parent;
    AppCompatButton request;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (CameraBtn) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register , container , false);

        name = (AppCompatEditText) view.findViewById(R.id.firstname);
        lastname = (AppCompatEditText) view.findViewById(R.id.lastname);
        kode = (AppCompatEditText) view.findViewById(R.id.kodmelli);
        education = (AppCompatEditText) view.findViewById(R.id.education);
        stock = (AppCompatEditText) view.findViewById(R.id.education_stock);
        profile = (CircleImageView) view.findViewById(R.id.profile);
        parent = (LinearLayoutCompat) view.findViewById(R.id.btnparen);
        gallery = (AppCompatImageButton) view.findViewById(R.id.gallery);
        camera = (AppCompatImageButton) view.findViewById(R.id.camera);
        request = (AppCompatButton) view.findViewById(R.id.request);

        callback.Camera(request,profile , parent , camera , gallery);

        windowManager = getActivity().getWindowManager();
        metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);

        name.addTextChangedListener(this);
        lastname.addTextChangedListener(this);
        kode.addTextChangedListener(this);
        stock.addTextChangedListener(this);
        education.addTextChangedListener(this);



        setupViewsMargin();

        setupwidthAndHeight();




        return view;
    }

    private void setupViewsMargin() {



        LinearLayoutCompat.LayoutParams nameparams = (LinearLayoutCompat.LayoutParams) name.getLayoutParams();
        nameparams.setMargins(0,metrics.heightPixels/40,0,0);
        name.setLayoutParams(nameparams);

        LinearLayoutCompat.LayoutParams lastnameparams = (LinearLayoutCompat.LayoutParams) lastname.getLayoutParams();
        lastnameparams.setMargins(0,metrics.heightPixels/40,0,0);
        lastname.setLayoutParams(lastnameparams);


        LinearLayoutCompat.LayoutParams kodeparams = (LinearLayoutCompat.LayoutParams) kode.getLayoutParams();
        kodeparams.setMargins(0,metrics.heightPixels/40,0,0);
        kode.setLayoutParams(kodeparams);


        LinearLayoutCompat.LayoutParams educationparams = (LinearLayoutCompat.LayoutParams) education.getLayoutParams();
        educationparams.setMargins(0,metrics.heightPixels/40,0,0);
        education.setLayoutParams(educationparams);

        LinearLayoutCompat.LayoutParams stockparams = (LinearLayoutCompat.LayoutParams) stock.getLayoutParams();
        stockparams.setMargins(0,metrics.heightPixels/40,0,0);
        stock.setLayoutParams(stockparams);


    }

    private void setupwidthAndHeight(){

        profile.getLayoutParams().height = metrics.heightPixels/ 6;
        profile.getLayoutParams().width = metrics.widthPixels/ 3;

        name.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        lastname.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        kode.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        education.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        stock.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        callback.ContentCallBack(name.getText().toString() ,
                kode.getText().toString() , lastname.getText().toString() , education.getText().toString() , stock.getText().toString());

    }
}
