package com.religiousschool;


import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class GroupValuesAdapter extends RecyclerView.Adapter<GroupValuesAdapter.TestViewHolder> {

    ArrayList<String> names ;
    AppCompatActivity activity;


    public GroupValuesAdapter(AppCompatActivity activity, ArrayList<String> names ){
        this.names = names;
        this.activity = activity;
    }

    @Override
    public TestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.groupadapter,parent,false);
        return new TestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TestViewHolder holder, final int position) {
        holder.tv.setText(names.get(position));
        holder.tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChildeTab childeTab = ChildeTab.newInstance(holder.tv.getText().toString());
                activity.getSupportFragmentManager().beginTransaction().replace(android.R.id.content,childeTab).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return names.size();
    }

    protected class TestViewHolder extends RecyclerView.ViewHolder{
        public TextView tv;
        public TestViewHolder(View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.names);
        }
    }

}
