package com.religiousschool;

import android.support.v7.widget.AppCompatButton;

public interface InputStudentCallBack {

    void nextButtonCallBack(AppCompatButton next);
}
