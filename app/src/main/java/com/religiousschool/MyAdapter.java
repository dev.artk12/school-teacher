package com.religiousschool;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageRequest;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.religiousschool.DetailePage.DetailActivity;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    List<ContentObject> objects ;
    Context context;
    String email;
    Picasso picasso;
    LruCache<Integer,Bitmap> imagecach;
    RequestOptions requestOptions;
    ImageRequest imgRequest;
    RequestQueue queue;
    int color;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;





    public MyAdapter(Context context , ArrayList objects , String email){

        if (context != null){
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
            editor = preferences.edit();
            int themepos = preferences.getInt("theme",0);




        }



        this.objects = objects;
        this.context = context;
        this.email = email;
        picasso = Picasso.get();
        int maxsize = (int) Runtime.getRuntime().maxMemory();
        imagecach = new LruCache<>(maxsize / 8);
        requestOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE) // because file name is always same
                .skipMemoryCache(true);


    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView name,stock,barcode ;
        public RelativeLayout forground;


        public ViewHolder(View Itemview){
            super(Itemview);
            name = itemView.findViewById(R.id.name);
            stock = itemView.findViewById(R.id.stock);
            barcode = itemView.findViewById(R.id.barcode);
            forground = itemView.findViewById(R.id.view_forground);


        }
    }


    @NonNull
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

            holder.name.setText(String.valueOf(objects.get(position).getName()));
            holder.stock.setText(objects.get(position).getStock()+objects.get(position).getStockFormat());
            holder.barcode.setText(objects.get(position).getBarCode());


            holder.forground.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtra("id",objects.get(position).getID());
                    context.startActivity(intent);
                }
            });

    }

    @Override
    public int getItemCount() {
        return objects.size();
    }



}
