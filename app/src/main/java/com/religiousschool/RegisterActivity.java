package com.religiousschool;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterActivity extends AppCompatActivity implements CameraBtn , WaitingCallBack ,detailstudiCallBack , InputStudentCallBack{


    private static final int REQUEST_OPEN_GALLETY = 105 ;
    private static final int REQUEST_IMAGE = 106 ;
    private static final String PROFILE_IMAGEVIEW = "prof";
    private static final String CAMERABUTTONS = "btns";
    private static final String NAMEKEY = "NameKey" ;
    private static final String KODEKEY = "KodeKey";
    private static final String LASTNAMEKEY = "LastNameKey" ;
    private static final String EDUCATIONKEY = "EducationKey";
    private static final String STOCKKEY = "StockKey";
    private static final String EDUCCATIONNAMEKEY = "EducationName";
    private static final String STUDENTSSTOCKKEY = "StudentStock";

    StartFragment fragment;
    Animation fadein;
    String [] permission = new String[]{Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE , Manifest.permission.READ_EXTERNAL_STORAGE};
    String camraPermisson = Manifest.permission.CAMERA;
    Uri uri;
    int color;
    Map<String , View> viewMap ;
    Map<String,String> result;
    int COUNTER = 1;
    int STUDENTCOUNTER = 1 ;
    InputStudiName dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        color = ActivityCompat.getColor(this , R.color.hintregistercolor);
        viewMap = new HashMap<>();
        result = new HashMap<>();

        fadein = AnimationUtils.loadAnimation(this,R.anim.fadein);

        fragment = new StartFragment();


        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.fadein, android.R.anim.slide_out_right);
        fragmentTransaction.replace(android.R.id.content, fragment, "h");
        fragmentTransaction.commit();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                fragmentTransaction.replace(android.R.id.content,new Register_Fragemnt_Start() , "h");
                fragmentTransaction.commit();

                finish();
                startActivity(new Intent(RegisterActivity.this,MainActivity.class));

            }
        }, 5000);

    }


    @Override
    public void Camera(final AppCompatButton request , final CircleImageView profile, final LinearLayoutCompat btnparent , AppCompatImageButton camera , AppCompatImageButton gallery ) {

        btnparent.setVisibility(View.INVISIBLE);
        final Animation ani = AnimationUtils.loadAnimation(RegisterActivity.this,R.anim.cameragalleryout);

        viewMap.put(PROFILE_IMAGEVIEW,profile);
        viewMap.put(CAMERABUTTONS , btnparent);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OpenCameraButtons(btnparent, profile, ani);

            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(RegisterActivity.this , camraPermisson) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(RegisterActivity.this , Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                    CameraRequest();
                }else {
                    ActivityCompat.requestPermissions(RegisterActivity.this , permission, 101);
                }
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(RegisterActivity.this , Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(RegisterActivity.this , Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Ucrop.openGalletry(RegisterActivity.this, REQUEST_OPEN_GALLETY);
                }else {
                    ActivityCompat.requestPermissions(RegisterActivity.this , permission, 102);
                }

            }
        });

        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (result.get(NAMEKEY) != null && result.get(KODEKEY)!= null
                        &&result.get(LASTNAMEKEY)!= null &&
                        result.get(EDUCATIONKEY)!= null && result.get(STOCKKEY)!= null ){

                    if (!result.get(NAMEKEY).isEmpty() && !result.get(KODEKEY).isEmpty()
                            &&!result.get(LASTNAMEKEY).isEmpty() &&
                            !result.get(EDUCATIONKEY).isEmpty() && !result.get(STOCKKEY).isEmpty()){


                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                        fragmentTransaction.replace(android.R.id.content,new Request_Waiting_Fragment() , "h");
                        fragmentTransaction.commit();


                    }else {
                        Toast.makeText(RegisterActivity.this, "لطفا تمامی فیلد ها را بر کنید", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(RegisterActivity.this, "لطفا تمامی فیلدها را بر کنید", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public void ContentCallBack(String Name, String Kode, String LastName, String education, String Stock) {
        result.put(NAMEKEY,Name);
        result.put(KODEKEY,Kode);
        result.put(LASTNAMEKEY,LastName);
        result.put(EDUCATIONKEY,education);
        result.put(STOCKKEY,Stock);


    }

    private void OpenCameraButtons(LinearLayoutCompat btnparent, CircleImageView profile, Animation ani) {
        Animation animation = AnimationUtils.loadAnimation(RegisterActivity.this,R.anim.cameragalleryin);
        btnparent.setAnimation(animation);
        btnparent.setVisibility(View.VISIBLE);
        profile.setAnimation(ani);
    }

    private void CloseCameraButtons(LinearLayoutCompat btnparent, CircleImageView profile) {
        Animation animation = AnimationUtils.loadAnimation(RegisterActivity.this,R.anim.cameragalleryout);
        btnparent.setAnimation(animation);
        btnparent.setVisibility(View.INVISIBLE);
        Animation anim = AnimationUtils.loadAnimation(RegisterActivity.this,R.anim.cameragalleryin);
        profile.setAnimation(anim);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 101 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            CameraRequest();
        }if (requestCode == 102 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Ucrop.openGalletry(RegisterActivity.this,REQUEST_OPEN_GALLETY);
        }


        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Ucrop ucrop = new Ucrop(color);
        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK){
            ucrop.startCropActivity(RegisterActivity.this,uri,"profile");
        }else if (requestCode == REQUEST_OPEN_GALLETY && resultCode == RESULT_OK){
            uri = data.getData();
            ucrop.startCropActivity(RegisterActivity.this,uri,"profile");
        }else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK){
            Bitmap bitmap = Ucrop.handleCropResult(RegisterActivity.this, data);
            CircleImageView profile = (CircleImageView) viewMap.get(PROFILE_IMAGEVIEW);
            profile.setImageBitmap(bitmap);
            CloseCameraButtons((LinearLayoutCompat) viewMap.get(CAMERABUTTONS),profile);


        }else if(requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_CANCELED){
            Toast.makeText(this, "cancel", Toast.LENGTH_SHORT).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void CameraRequest() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent CamIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()+"/inventory/Image/",
                "file"+String.valueOf(System.currentTimeMillis())+".jpg");
        uri = Uri.fromFile(file);
        CamIntent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
        CamIntent.putExtra("return-data",true);
        startActivityForResult(CamIntent,REQUEST_IMAGE);


    }

    @Override
    public void DoneButton(AppCompatButton done) {
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog = InputStudiName.newInstance(result.get(STOCKKEY),COUNTER);
                dialog.setCancelable(false);
                dialog.show(getSupportFragmentManager(),"Input studi");

                COUNTER++;
            }
        });
    }

    @Override
    public void DoneButtonCallBack(AppCompatButton done) {

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (result.get(EDUCCATIONNAMEKEY) != null && result.get(STUDENTSSTOCKKEY) != null && !result.get(EDUCCATIONNAMEKEY).isEmpty()&& !result.get(STUDENTSSTOCKKEY).isEmpty()){

                    if (Integer.valueOf(result.get(STUDENTSSTOCKKEY)) > 0){

                        if (!result.get(EDUCCATIONNAMEKEY).isEmpty() && !result.get(STUDENTSSTOCKKEY).isEmpty() ){

                            FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            InputStudentFragment fragment = InputStudentFragment.newInstance(result.get(EDUCCATIONNAMEKEY) , STUDENTCOUNTER);
                            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                            fragmentTransaction.replace(android.R.id.content,fragment, "h");
                            fragmentTransaction.commit();

                            STUDENTCOUNTER++;

                            dialog.dismiss();


                        }else {
                            Toast.makeText(RegisterActivity.this, "لطفا تمامی فیلدها را پر کنید.", Toast.LENGTH_SHORT).show();
                        }
                    }else if (Integer.valueOf(result.get(STUDENTSSTOCKKEY)) <= 0) {
                        Toast.makeText(RegisterActivity.this, "دانش آموزان باید بیش از 1 نفر باشند.", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(RegisterActivity.this, "لطفا تمامی فیلدها را پر کنید.", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public void InputsCallBack(String name, String students) {

        result.put(EDUCCATIONNAMEKEY , name);
        result.put(STUDENTSSTOCKKEY , students);
    }

    @Override
    public void nextButtonCallBack(AppCompatButton next) {

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (STUDENTCOUNTER <= Integer.valueOf(result.get(STUDENTSSTOCKKEY)) && Integer.valueOf(result.get(STUDENTSSTOCKKEY)) != 0){

                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    InputStudentFragment fragment = InputStudentFragment.newInstance(result.get(EDUCCATIONNAMEKEY) , STUDENTCOUNTER);
                    fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    fragmentTransaction.replace(android.R.id.content,fragment, "h");
                    fragmentTransaction.commit();

                    STUDENTCOUNTER++;

                }else if (STUDENTCOUNTER > Integer.valueOf(result.get(STUDENTSSTOCKKEY))){
                    if (COUNTER <= Integer.valueOf(result.get(STOCKKEY))){
                        STUDENTCOUNTER = 1;

                        dialog = InputStudiName.newInstance(result.get(STOCKKEY),COUNTER);
                        dialog.setCancelable(false);
                        dialog.show(getSupportFragmentManager(),"Input studi");

                        COUNTER++;
                    }else {
                        finish();
                        Intent intent = new Intent(RegisterActivity.this , MainActivity.class);
                        startActivity(intent);
                    }
                }

            }
        });


    }
}
