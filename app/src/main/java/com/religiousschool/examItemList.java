package com.religiousschool;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class examItemList extends RecyclerView.Adapter<examItemList.ViewHolder>{


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.examslistitems,viewGroup,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    protected class ViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView Name , Date ;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            Date = (AppCompatTextView) itemView.findViewById(R.id.dateexam);
            Name = (AppCompatTextView) itemView.findViewById(R.id.nameeducation);
        }
    }




}
